﻿using UnityEngine;
using System.Collections;


//Enemy inherits from MovingObject, our base class for objects that can move, Player also inherits from this.
public class Enemy : MovingObjects
{
	public float CurrentHp;
	public float MaxHp;
	public float ATK;
		
		
	//private Animator animator;							//Variable of type Animator to store a reference to the enemy's Animator component.
	private Transform target;							//Transform to attempt to move toward each turn.
	private bool skipMove;                              //Boolean to determine whether or not enemy should skip a turn or move this turn.

	private bool mEnemyMoving;
	private float mTurnDelay = 0.1f;
	//==========================================================================================================

	//Start overrides the virtual Start function of the base class.
	protected override void Start ()
	{
		CurrentHp += Mathf.Log(GameManagers.Instance.MapInfo.Map1.RoomLevel, 2f)*10f;
		MaxHp += Mathf.Log(GameManagers.Instance.MapInfo.Map1.RoomLevel, 2f)*10f;
		ATK += Mathf.Log(GameManagers.Instance.MapInfo.Map1.RoomLevel, 2f);

		//Register this enemy with our instance of GameManager by adding it to a list of Enemy objects. 
		//This allows the GameManager to issue movement commands.
		GameManagers.Instance.EnemyInfo.EnemiesList.Add(this);

		//Get and store a reference to the attached Animator component.
		//animator = GetComponent<Animator> ();

		//Find the Player GameObject using it's tag and store a reference to its transform component.
		//target = GameObject.FindGameObjectWithTag ("Player").transform;
		if (GameManagers.Instance.GMState.GameStartInWhere == GameDataStruct.AllMainScenesList.SceneS2)
		{
			GameObject player = GameManagers.Instance.CommunFunc.FindDontDestroyObjectByName(GameDataStruct.AllCharactersList.S2Player.ToString());
			target = player.transform;
		}
			
		//Call the start function of our base class MovingObject.
		base.Start ();
	}

    private void Update()
    {
		//Debug.Log($"enemy count={GameManagers.Instance.EnemyInfo.EnemiesList.Count}");
		if (mEnemyMoving /*|| GameManagers.Instance.EnemyInfo.PlayersTurn*/)
			return;
		StartCoroutine(MoveEnemies());
	}

    private void OnDestroy()
    {
        int i = 0;
        foreach (Enemy enemy in GameManagers.Instance.EnemyInfo.EnemiesList)
        {
            //Debug.Log($"OnDestroy: enemy ID={enemy.GetInstanceID()}, this ID={this.GetInstanceID()}");
			if (enemy.GetInstanceID() == this.GetInstanceID())
			{
				//Destroy(this);
				GameManagers.Instance.EnemyInfo.EnemiesList.RemoveAt(i);
				break;
			}
            i++;
        }
    }
	//==========================================================================================================
	IEnumerator MoveEnemies()
	{
		//While enemiesMoving is true player is unable to move.
		mEnemyMoving = true;

		//Wait for turnDelay seconds, defaults to .1 (100 ms).
		yield return new WaitForSeconds(mTurnDelay);

		MoveEnemy();
		//Wait for Enemy's moveTime before moving next Enemy, 
		yield return new WaitForSeconds(1f/MoveSpeed);

		//Once Enemies are done moving, set playersTurn to true so player can move.
		//GameManagers.Instance.EnemyInfo.PlayersTurn = true;

		//Enemies are done moving, set enemiesMoving to false.
		mEnemyMoving = false;
	}
	//==========================================================================================================


	//Override the AttemptMove function of MovingObject to include functionality needed for Enemy to skip turns.
	//See comments in MovingObject for more on how base AttemptMove function works.
	protected override void AttemptMove <T> (int xDir, int yDir)
	{
		//Check if skipMove is true, if so set it to false and skip this turn.
		if(skipMove)
		{
			skipMove = false;
			return;
				
		}
			
		//Call the AttemptMove function from MovingObject.
		base.AttemptMove <T> (xDir, yDir);
			
		//Now that Enemy has moved, set skipMove to true to skip next move.
		skipMove = true;
	}
		
	//MoveEnemy is called by the GameManger each turn to tell each Enemy to try to move towards the player.
	public void MoveEnemy ()
	{
		//Declare variables for X and Y axis move directions, these range from -1 to 1.
		//These values allow us to choose between the cardinal directions: up, down, left and right.
		int xDir = 0;
		int yDir = 0;

		//If the difference in positions is approximately zero (Epsilon) do the following:
		if(Mathf.Abs (target.position.x - transform.position.x) < float.Epsilon)
				
			//If the y coordinate of the target's (player) position is greater than the y coordinate of this enemy's position set y direction 1 (to move up). If not, set it to -1 (to move down).
			yDir = target.position.y > transform.position.y ? 1 : -1;
			
		//If the difference in positions is not approximately zero (Epsilon) do the following:
		else
			//Check if target x position is greater than enemy's x position, if so set x direction to 1 (move right), if not set to -1 (move left).
			xDir = target.position.x > transform.position.x ? 1 : -1;
			
		//Call the AttemptMove function and pass in the generic parameter Player, because Enemy is moving and expecting to potentially encounter a Player
		AttemptMove <Player> (xDir, yDir);
	}
		
	//OnCantMove is called if Enemy attempts to move into a space occupied by a Player, it overrides the OnCantMove function of MovingObject 
	//and takes a generic parameter T which we use to pass in the component we expect to encounter, in this case Player
	protected override void OnCantMove <T> (T component)
	{
		//Debug.Log($"Enemy({this.transform.position.x},{this.transform.position.y}) hit target({component.name}) at ({component.transform.position.x},{component.transform.position.y})");

		//Declare hitPlayer and set it to equal the encountered component.
		Player hitPlayer = component as Player;
		hitPlayer.PlayerBeHitted(this.ATK);
			
		//Call the LoseFood function of hitPlayer passing it playerDamage, the amount of foodpoints to be subtracted.
//??		hitPlayer.LoseFood (playerDamage);
			
		//Set the attack trigger of animator to trigger Enemy attack animation.
		//animator.SetTrigger ("enemyAttack");

	}
	public void EnemyBeHitted(float atk)
	{
		CurrentHp -= atk;
		CurrentHp = Mathf.Clamp(CurrentHp, 0, MaxHp);
		Debug.Log($"Enemy HP={CurrentHp}");
		if (CurrentHp <= 0)
			this.gameObject.SetActive(false);
	}
	protected override void MovingEventAnime(bool isMoving, int horizontal, int vertical)
    {
        //throw new System.NotImplementedException();
    }
}
