﻿using System.Collections;
using UnityEngine;


public abstract class MovingObjects : MonoBehaviour
{
    public float MoveSpeed = 10f;
    public LayerMask BlockingLayer;

    protected Rigidbody2D mRigidBody;
    //private CircleCollider2D mCircleCollider;
    private BoxCollider2D mBoxCollider;

    protected bool mIsMoving = false;
    protected bool mUrgentStopMove = false;
    //================================================================

    protected virtual void Start()
    {
        //Debug.Log("MovingObjects start");
        mRigidBody = GetComponent<Rigidbody2D>();
        //mCircleCollider = GetComponent<CircleCollider2D>();
        mBoxCollider = GetComponent<BoxCollider2D>();
    }

    protected bool Move(int xDir, int yDir, out RaycastHit2D hit)
    {
        Vector2 start = transform.position; //object location
        Vector2 end = start + new Vector2(xDir, yDir);

        //Debug.Log("Move from(" + start + ") end(" + end + ")");
        //mCircleCollider.enabled = false;
        mBoxCollider.enabled = false;
        hit = Physics2D.Linecast(start, end, BlockingLayer);
        //Debug.Log("hit position: "+ hit.transform + "moving="+mIsMoving);
        //mCircleCollider.enabled = true;
        mBoxCollider.enabled = true;

        if (hit.transform == null && !mIsMoving)
        {
            StartCoroutine(SmoothMovement(end));
            return true;
        }

        return false;
    }

    protected IEnumerator SmoothMovement(Vector3 end)
    {
        mIsMoving = true;
        //Debug.Log($"-> Is moving = {mIsMoving}");
        MovingEventAnime(mIsMoving, (int)(end.x - transform.position.x), (int)(end.y - transform.position.y));
        float distance = (transform.position - end).sqrMagnitude;
        //Debug.Log("org distance=" + distance);
        while(distance > float.Epsilon)
        {
            if (mUrgentStopMove == true)
            {
                mUrgentStopMove = false;
                break;
            }
            Vector3 newPostion = Vector3.MoveTowards(mRigidBody.position, end, MoveSpeed * Time.deltaTime);
            mRigidBody.MovePosition(newPostion);
            distance = (transform.position - end).sqrMagnitude;
            //Debug.Log("moving, distance="+distance);
            yield return 0;
        }
        mRigidBody.MovePosition(end);
        mIsMoving = false;
        MovingEventAnime(mIsMoving, (int)(end.x - transform.position.x), (int)(end.y - transform.position.y));
        //Debug.Log($"<- Is moving = {mIsMoving}");
    }

    protected virtual void NormalMove(int xDir, int yDir)
    {
        
    }
    protected virtual void AttemptMove<T>(int xDir, int yDir) where T : Component
    {
        RaycastHit2D hit;

        bool canMove = Move(xDir, yDir, out hit);
        if (hit.transform == null)
            return;
        
        T hitComponent = hit.transform.GetComponent<T>();
        if (!canMove && hitComponent != null)
        {
            OnCantMove(hitComponent);
        }
    }

    protected abstract void MovingEventAnime(bool isMoving, int horizontal, int vertical);
    protected abstract void OnCantMove<T>(T component) where T : Component;
}
