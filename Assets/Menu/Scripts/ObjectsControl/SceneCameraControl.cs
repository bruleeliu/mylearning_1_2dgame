﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneCameraControl : MonoBehaviour
{
    private bool mLookAtObjectFound = false;
    private GameObject mLookAtObject = null;

    private float DefaultZoom = 3.5f;
    private bool BoundaryCheck = true;
    private Vector3 minimum = new Vector3(-6.6f, -3.6f, 0f);
    private Vector3 maximum = new Vector3(8f, 4f, 0f);
    public bool IsDontDestroyObject = false;
    //====================================================================
    private void Awake()
    {
        if (IsDontDestroyObject == true)
            RegisterSceneCameraControlOnLoaded();
    }
    void Start()
    {
        //Debug.Log($"{this.name} on Start");
        if (IsDontDestroyObject != true)
            SceneCameraInitForCommon();
    }
    void Update()
    {
        SceneCameraViewUpdate();
    }
    //====================================================================
    protected void RegisterSceneCameraControlOnLoaded()
    {
        //Debug.Log("RegisterCameraControlOnLoaded");
        SceneManager.sceneLoaded += SceneCameraControlOnLoaded; //add register event on each scene load
    }

    void SceneCameraControlOnLoaded(UnityEngine.SceneManagement.Scene scene, LoadSceneMode mode)
    {
        // Remove it for each screen load this when this object used DontDestroyOnLoad.
        //SceneManager.sceneLoaded -= GameUIOnLoaded;

        // To Do...
        //Debug.Log($"{this.name} on SceneCameraControlOnLoaded");
        if (GameManagers.Instance == null)
            return;
        if (GameManagers.Instance.GMState.GameStartInWhere == GameDataStruct.AllMainScenesList.SceneS2)
        {
            if (GameManagers.Instance.CommunFunc.FindDontDestroyObjectByName(GameDataStruct.AllGameUIList.PlayerUI.ToString()) == null) 
            {
                SceneManager.sceneLoaded -= SceneCameraControlOnLoaded;
                return;
            }
            SceneCameraInitForCommon();
            
            if (scene.name == GameDataStruct.AllSubScenesList.ScenesS2_InMainTown.ToString())
            {
                DefaultZoom = 6.5f;
                minimum = new Vector3(-3.7f, -2f, 0f);
                maximum = new Vector3(5.7f, 3f, 0f);
                this.GetComponent<Camera>().orthographicSize = DefaultZoom;
                //Debug.Log($"boundary check={BoundaryCheck}, min={minimum}, max={maximum}");
            }
            if(scene.name == GameDataStruct.AllSubScenesList.ScenesS2_InMonsterRoom.ToString())
            {
                DefaultZoom = 7f;
                this.GetComponent<Camera>().orthographicSize = DefaultZoom;
            }
        }
    }


    protected void SceneCameraDefaultValue()
    {
        mLookAtObjectFound = false;
        mLookAtObject = null;

        DefaultZoom = 3.5f;
        BoundaryCheck = true;
        minimum = new Vector3(-7.2f, -4f, 0f);
        maximum = new Vector3(9.1f, 5f, 0f);
        //LookAtScene = false;
}

    protected void SceneCameraInitForCommon()
    {
        if (GameManagers.Instance == null)
            return;

        SceneCameraDefaultValue(); // Default is no look at object found

        if(GameManagers.Instance.GMState.GameStartInWhere == GameDataStruct.AllMainScenesList.SceneS2)
        {
            // Look at player at first
            mLookAtObject = GameManagers.Instance.CommunFunc.FindDontDestroyObjectByName(GameDataStruct.AllCharactersList.S2Player.ToString());

            GameDataStruct.AllSubScenesList currentSceneEnum = GameManagers.Instance.CommunFunc.GetEnumBySubScenesName(SceneManager.GetActiveScene().name);
            switch (currentSceneEnum)
            {
                case GameDataStruct.AllSubScenesList.ScenesS2_InMonsterRoom:
                    float roomColumn = GameManagers.Instance.MapInfo.Map1.RoomSize.Columns;
                    float roomRow = GameManagers.Instance.MapInfo.Map1.RoomSize.Rows;
                    if (roomColumn <= 8 && roomRow <= 8) 
                    {
                        this.transform.position = new Vector3(3.5f, 3.5f, this.transform.position.z);
                        DefaultZoom = 5f;
                        this.GetComponent<Camera>().orthographicSize = DefaultZoom;
                        mLookAtObjectFound = false;
                        BoundaryCheck = false;
                    }
                    else
                    {
                        if (mLookAtObject != null)
                        {
                            // Found player
                            DefaultZoom = 4f;
                            this.GetComponent<Camera>().orthographicSize = DefaultZoom;
                            mLookAtObjectFound = true;
                            BoundaryCheck = false;
                        }
                    }
                    break;
                case GameDataStruct.AllSubScenesList.ScenesS2_InHouse:
                    if (mLookAtObject != null)
                    {
                        // Look at scene
                        GameDataStruct.AllPortalsList portalEnum = mLookAtObject.GetComponent<Player>().FromPortal;
                        //Debug.Log($"-> from porttal: {portalEnum.ToString()}");

                        GameObject targetObject = GameObject.Find(portalEnum.ToString());
                        if (targetObject == null)
                        {
                            break;
                        }
                        //Debug.Log($"-> portal object {targetObject.name} found");
                        mLookAtObject = targetObject.transform.GetChild(0).gameObject;
                        //Debug.Log($"-> neww look at object {mLookAtObject.name} found");
                        this.GetComponent<Camera>().orthographicSize = 4f;
                        mLookAtObjectFound = true;
                        BoundaryCheck = false;
                    }
                    break;
                case GameDataStruct.AllSubScenesList.ScenesS2_Init:
                case GameDataStruct.AllSubScenesList.ScenesS2_InMainTown:
                    if (mLookAtObject != null)
                    {
                        // Found player
                        this.GetComponent<Camera>().orthographicSize = DefaultZoom;
                        mLookAtObjectFound = true;
                    }
                    break;
                default:
                    mLookAtObjectFound = false;
                    break;
            }
        }
        
    }

    protected void SceneCameraViewUpdate()
    {
        if (mLookAtObjectFound == false)
            return;
        if (mLookAtObject == null)
            return;
        if (GameManagers.Instance == null)
            return;

        //Debug.Log($"{this.name} on SceneCameraViewUpdate");
        Vector3 tempLoc = mLookAtObject.transform.position;
        if(BoundaryCheck == true)
            tempLoc = GameManagers.Instance.CommunFunc.BoundaryCheck2D(tempLoc, minimum, maximum);
        tempLoc.z = this.transform.position.z;
        this.transform.position = tempLoc;
    }
    //====================================================================
    //====================================================================
}
