﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Player : MovingObjects
{
    public bool TriggerEnable;
    // player needs to update when any trigger
    public string Name;
    public Vector2 Hp; //(current, max)
    public Vector2 Mp; //(current, max)
    [SerializeField]public GameDataStruct.CharactersData.Attributes Others;
    public Vector3 Location;
    public GameDataStruct.AllMainScenesList MainScene;
    public GameDataStruct.AllSubScenesList FromSubScene;
    public GameDataStruct.AllPortalsList FromPortal;

    private Animator mAnimator;
    private bool skipMove;
    //private bool mPlayerMoving;
    //====================================================================
    private void Awake()
    {
        //RegisterPlayerOnLoaded();
    }

    protected override void Start()
    {
        //Debug.Log("Player stert");
        if (GameManagers.Instance != null)
        {
            float x = transform.position.x;
            if (x > (int)x) x = (int)x + 1f;
            float y = transform.position.y;
            if (y > (int)y) y = (int)y + 1f;
            Location = new Vector3(x, y, transform.position.z);
            GameDataStruct.CharactersData data = new GameDataStruct.CharactersData(Name, Hp, Mp, Others, Location, MainScene, FromSubScene, FromPortal);
            GameManagers.Instance.PlayerInfo.SetData(data);
        }
        mAnimator = GetComponent<Animator>();

        base.Start();
    }

    protected void Update()
    {
        //if (SceneManager.GetActiveScene().name == GameDataStruct.AllSubScenesList.ScenesS2_InMonsterRoom.ToString())
            //if (!GameManagers.Instance.EnemyInfo.PlayersTurn) return;
        HandleMovement();
    }

    private void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex, LoadSceneMode.Single);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //Debug.Log("Player trigger (collision, this)=(" + collision.name+", "+this.name+")");

        if (TriggerEnable == false)
            return;

        if (GameManagers.Instance == null)
            return;

        switch(this.MainScene)
        {
            case GameDataStruct.AllMainScenesList.SceneS2:
                if (collision.tag == "Food" )
                {
                    if (Hp.x != Hp.y)
                    {
                        Hp.x += Hp.y * 0.05f;
                        Hp.x = Hp.x >= Hp.y ? Hp.y : Hp.x;
                    }
                    collision.gameObject.SetActive(false);
                }
                if (collision.tag == "Soda")
                {
                    if (Hp.x != Hp.y)
                    {
                        Hp.x += Hp.y * 0.1f;
                        Hp.x = Hp.x >= Hp.y ? Hp.y : Hp.x;
                    }
                    if (Mp.x != Mp.y)
                    {
                        Mp.x += Mp.y * 0.1f;
                        Mp.x = Mp.x >= Mp.y ? Mp.y : Mp.x;
                    }
                    collision.gameObject.SetActive(false);
                }
                if (collision.tag == "Exit")
                {
                    mUrgentStopMove = true;
                    //this.gameObject.layer = "Player";
                    Location = new Vector3((int)transform.position.x, (int)transform.position.y, transform.position.z);
                    FromPortal = GameDataStruct.AllPortalsList.MonsterRoomPortal;
                    FromSubScene = GameDataStruct.AllSubScenesList.ScenesS2_InMonsterRoom;
                    GameDataStruct.CharactersData tempData = new GameDataStruct.CharactersData(Name, Hp, Mp, Others, Location, MainScene, FromSubScene, FromPortal);
                    GameManagers.Instance.PlayerInfo.SetData(tempData);
                    GameManagers.Instance.gameObject.GetComponent<ScenesSwitch>().GoToNextScene(GameDataStruct.AllSubScenesList.ScenesS2_InMainTown.ToString(), false);
                }
                if (collision.tag == "Next")
                {
                    mUrgentStopMove = true;
                    //GameManagers.Instance.GMState.DoingSetup = true;
                    GameManagers.Instance.MapInfo.Map1.RoomLevel++;
                    SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
                    //Invoke("Restart", 1f);

                }
                if (collision.tag == "Portal")
                {
                    string beTriggeredName = collision.name;
                    GameDataStruct.AllPortalsList portalEnum = GameManagers.Instance.CommunFunc.GetEnumByPortalName(beTriggeredName); /// current triggered portal

                    if (portalEnum != GameDataStruct.AllPortalsList.Unknown || portalEnum != GameDataStruct.AllPortalsList.End)
                    {
                        mUrgentStopMove = true;
                        //Debug.Log("urgent stop set");
                        string currentSceneName = SceneManager.GetActiveScene().name;
                        GameDataStruct.AllSubScenesList sceneEnum = GameManagers.Instance.CommunFunc.GetEnumBySubScenesName(currentSceneName); /// current triggered scene

                        Location = new Vector3((int)transform.position.x, (int)transform.position.y, transform.position.z);
                        FromPortal = portalEnum;
                        FromSubScene = sceneEnum;
                        GameDataStruct.CharactersData tempData = new GameDataStruct.CharactersData(Name, Hp, Mp, Others, Location, MainScene, FromSubScene, FromPortal);
                        GameManagers.Instance.PlayerInfo.SetData(tempData);

                        switch (sceneEnum)//current scene
                        {
                            case GameDataStruct.AllSubScenesList.ScenesS2_InMainTown:
                                if (portalEnum == GameDataStruct.AllPortalsList.HomePortal)
                                {
                                    GameManagers.Instance.CommunFunc.RemoveDontDestroyObjectByName(GameDataStruct.AllCharactersList.S2Player.ToString());
                                    //SceneManager.sceneLoaded -= PlayerOnLoaded; 
                                    GameManagers.Instance.gameObject.GetComponent<ScenesSwitch>().Menu();
                                }
                                else if (portalEnum == GameDataStruct.AllPortalsList.MonsterRoomPortal)
                                {
                                    GameManagers.Instance.gameObject.GetComponent<ScenesSwitch>().GoToNextScene(GameDataStruct.AllSubScenesList.ScenesS2_InMonsterRoom.ToString(), false);
                                }
                                else
                                {
                                    //goto item, armor, weapon portal...
                                    GameManagers.Instance.gameObject.GetComponent<ScenesSwitch>().GoToNextScene(GameDataStruct.AllSubScenesList.ScenesS2_InHouse.ToString(), false);
                                }
                                break;
                            case GameDataStruct.AllSubScenesList.ScenesS2_InMonsterRoom:
                            case GameDataStruct.AllSubScenesList.ScenesS2_InHouse:
                                GameManagers.Instance.gameObject.GetComponent<ScenesSwitch>().GoToNextScene(GameDataStruct.AllSubScenesList.ScenesS2_InMainTown.ToString(), false);
                                break;
                            default:
                                break;
                        }
                    }
                    else
                        Debug.Log("player trigger unknown portal");
                }
                break;
            default:
                break;
        }
    }
    //====================================================================

    public void RegisterPlayerOnLoaded()
    {
        //Debug.Log("RegisterPlayerOnLoaded");
        SceneManager.sceneLoaded += PlayerOnLoaded; //add register event on each scene load
    }

    public void PlayerBeHitted(float atk)
    {
        Hp.x -= atk;
        Hp.x = Mathf.Clamp(Hp.x, 0, Hp.y);
        Debug.Log($"Player HP={Hp.x}");
        if (Hp.x <= 0)
            this.gameObject.SetActive(false);
    }
    //====================================================================
    void PlayerOnLoaded(UnityEngine.SceneManagement.Scene scene, LoadSceneMode mode)
    {
        // Remove it for each screen load this when this object used DontDestroyOnLoad.
        //SceneManager.sceneLoaded -= PlayerOnLoaded; 

        // To Do...
    }
    void HandleMovement()
    {
        //mPlayerMoving = true;
        int horizontal = (int)(Input.GetAxisRaw("Horizontal"));
        int vertical = (int)(Input.GetAxisRaw("Vertical"));

        if (horizontal != 0)
            vertical = 0;


        if (horizontal != 0 || vertical != 0)
        {
            AttemptMove<Enemy>(horizontal, vertical);
            //NormalMove(horizontal, vertical);
        }
        //mPlayerMoving = false;
    }
    protected override void NormalMove(int xDir, int yDir)
    {
        RaycastHit2D hit;
        bool canMove = Move(xDir, yDir, out hit);
        //if (!canMove && hit.transform != null)
        //{

        //}
        //if(SceneManager.GetActiveScene().name == GameDataStruct.AllSubScenesList.ScenesS2_InMonsterRoom.ToString())
        //    GameManagers.Instance.EnemyInfo.PlayersTurn = false;
    }
    protected override void MovingEventAnime(bool isMoving, int horizontal, int vertical)
    {
        mAnimator.SetBool("IsMoving", isMoving);
        if (horizontal != 0 || vertical != 0)
        {
            mAnimator.SetInteger("Horizontal", horizontal);
            mAnimator.SetInteger("Vertical", vertical);
        }
    }
    protected override void AttemptMove<T>(int xDir, int yDir)
    {
        if (skipMove)
        {
            skipMove = false;
            return;

        }
        base.AttemptMove<T>(xDir, yDir);
        //GameManagers.Instance.EnemyInfo.PlayersTurn = false;
        skipMove = true;
    }
    protected override void OnCantMove<T>(T component)
    {
        //throw new NotImplementedException();
        //Debug.Log($"Player({this.transform.position.x},{this.transform.position.y}) hit target({component.name}) at ({component.transform.position.x},{component.transform.position.y})");
        Enemy hitEnemy = component as Enemy;
        hitEnemy.EnemyBeHitted(this.Others.Atk);
    }

    //=====================================================================
    //=====================================================================
}
