﻿using System.Collections;
using System.Collections.Generic;
//using UnityEditor.U2D.Animation;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameUI : MonoBehaviour
{  
    protected GameObject[] arrowList;
    protected GameObject canvasUI;
    protected GameObject currentLocation;
    protected GameObject nameText;
    protected GameObject hpBar;
    protected GameObject mpBar;
    //===================================================================
    protected void RegisterGameUIOnLoaded()
    {
        //Debug.Log("RegisterGameUIOnLoaded");
        SceneManager.sceneLoaded += GameUIOnLoaded; //add register event on each scene load
    }
    void GameUIOnLoaded(UnityEngine.SceneManagement.Scene scene, LoadSceneMode mode)
    {
        // Remove it for each screen load this when this object used DontDestroyOnLoad.
        //SceneManager.sceneLoaded -= GameUIOnLoaded;

        //To Do........
        //Debug.Log($"Game UI ({this.name}) on load");
        if (GameManagers.Instance == null)
            return;
        if (GameManagers.Instance.CommunFunc.FindDontDestroyObjectByName(GameDataStruct.AllCharactersList.S2Player.ToString()) == null)
        {
            Debug.Log($"Game UI ({GameDataStruct.AllCharactersList.S2Player.ToString()}) remove");
            GameManagers.Instance.CommunFunc.RemoveDontDestroyObjectByName(GameDataStruct.AllGameUIList.PlayerUI.ToString());
            SceneManager.sceneLoaded -= GameUIOnLoaded;
            return;
        }
        //arrowList = new GameObject[4]{ GameObject.Find("A"), GameObject.Find("D"), GameObject.Find("S"), GameObject.Find("W") };
        InitGameObject();
        UpdateUIState();
        UpdateSceneName(scene.name);

    }
    //===================================================================

    void Start()
    {
        RegisterGameUIOnLoaded();

        InitGameObject();
        UpdateUIState();
        UpdateSceneName(SceneManager.GetActiveScene().name);
    }
    private void FixedUpdate()
    {
        UpdateUIState();
        UpdateArrowKey();
    }

    //======================================================================================
    void InitGameObject()
    {
        arrowList = new GameObject[4] { GameObject.Find("A"), GameObject.Find("D"), GameObject.Find("S"), GameObject.Find("W") };
        canvasUI = GameObject.Find("CanvasUI");
        currentLocation = GameObject.Find("CurrentLocation");
        nameText = GameObject.Find("NameText");
        hpBar = GameObject.Find("HpBar");
        mpBar = GameObject.Find("MpBar");
    }
    void UpdateUIState()
    {
        if (this.name == GameDataStruct.AllGameUIList.PlayerUI.ToString())
        {
            //GameObject canvasUI = GameObject.Find("CanvasUI");
            ////GameObject currentLocation = GameObject.Find("CurrentLocation");
            //GameObject nameText = GameObject.Find("NameText");
            //GameObject hpBar = GameObject.Find("HpBar");
            //GameObject mpBar = GameObject.Find("MpBar");


            // Init minimap canvas
            canvasUI.gameObject.GetComponent<Canvas>().renderMode = RenderMode.ScreenSpaceCamera;
            canvasUI.gameObject.GetComponent<Canvas>().worldCamera = Camera.main.GetComponent<Camera>();

            // Init current location name
            //currentLocation.GetComponent<Text>().text = SceneManager.GetActiveScene().name;
            //UpdateSceneName(SceneManager.GetActiveScene().name);

            // Init left-top UI
            if (GameManagers.Instance != null)
            {
                GameObject player = GameManagers.Instance.CommunFunc.FindDontDestroyObjectByName(GameDataStruct.AllCharactersList.S2Player.ToString());
                if (player != null)
                {
                    Player playerData = player.GetComponent<Player>();
                    //=====
                    // Update name
                    nameText.GetComponent<Text>().text = playerData.Name;

                    // Update HP
                    float percentage = (playerData.Hp.x / playerData.Hp.y);
                    hpBar.GetComponent<Image>().fillAmount = percentage;
                    percentage *= 100f;
                    hpBar.transform.GetChild(0).gameObject.GetComponent<Text>().text = percentage.ToString("F") + "%";
                    if (percentage < 50f)
                        hpBar.transform.GetChild(0).gameObject.GetComponent<Text>().color = Color.black;
                    else
                        hpBar.transform.GetChild(0).gameObject.GetComponent<Text>().color = Color.white;

                    // Update MP
                    percentage = (playerData.Mp.x / playerData.Mp.y);
                    mpBar.GetComponent<Image>().fillAmount = percentage;
                    percentage *= 100f;
                    mpBar.transform.GetChild(0).gameObject.GetComponent<Text>().text = percentage.ToString("F") + "%";
                    if (percentage < 50f)
                        mpBar.transform.GetChild(0).gameObject.GetComponent<Text>().color = Color.black;
                    else
                        mpBar.transform.GetChild(0).gameObject.GetComponent<Text>().color = Color.white;

                    //=====
                }
            }
        }
    }
    void UpdateArrowKey()
    {
        int horizontal = (int)(Input.GetAxisRaw("Horizontal"));
        int vertical = (int)(Input.GetAxisRaw("Vertical"));
        bool currentArrowKeyUp = false;

        if (horizontal == 0 && vertical == 0)
            currentArrowKeyUp = true;

        string currentArrowKey = "";
        if (horizontal == 1) currentArrowKey = "D";
        else if (horizontal == -1) currentArrowKey = "A";
        else if (vertical == 1) currentArrowKey = "W";
        else if (vertical == -1) currentArrowKey = "S";

        for (int i = 0; i < arrowList.Length; i++)
        {
            if (arrowList[i].gameObject != null)
            {
                arrowList[i].gameObject.GetComponent<Image>().color = Color.white;
                if (currentArrowKeyUp == false && arrowList[i].transform.name == currentArrowKey)
                {
                    arrowList[i].gameObject.GetComponent<Image>().color = Color.yellow;
                }
            }
        }
    }
    void UpdateSceneName(string sceneName)
    {
        if (GameManagers.Instance == null)
            return;
        //GameObject currentLocation = GameObject.Find("CurrentLocation");
        GameDataStruct.AllSubScenesList currentEnum = GameManagers.Instance.CommunFunc.GetEnumBySubScenesName(sceneName);
        switch(currentEnum)
        {
            case GameDataStruct.AllSubScenesList.ScenesS2_InMainTown:
                currentLocation.GetComponent<Text>().text = "Main Town";
                break;
            case GameDataStruct.AllSubScenesList.ScenesS2_InMonsterRoom:
                currentLocation.GetComponent<Text>().text = "Monster Room LV. " + GameManagers.Instance.MapInfo.Map1.RoomLevel;
                break;
            case GameDataStruct.AllSubScenesList.ScenesS2_InHouse:
                GameDataStruct.CharactersData charactersData = GameManagers.Instance.PlayerInfo.GetData();
                if (charactersData.FromPortal == GameDataStruct.AllPortalsList.ItemPortal)
                    currentLocation.GetComponent<Text>().text = "Item House";
                else if (charactersData.FromPortal == GameDataStruct.AllPortalsList.ArmorPortal)
                    currentLocation.GetComponent<Text>().text = "Armor House";
                else if (charactersData.FromPortal == GameDataStruct.AllPortalsList.WeaponPortal)
                    currentLocation.GetComponent<Text>().text = "Weapon House";
                break;
            default:
                currentLocation.GetComponent<Text>().text = sceneName;
                break;
        }
        
    }

    

    //======================================================================================
}
