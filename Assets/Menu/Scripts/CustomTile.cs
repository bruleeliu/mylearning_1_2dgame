﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

#if UNITY_EDITOR
using UnityEditor;
#endif

[Serializable]
public class CustomTile : TileBase
{
    public Color customColor = Color.black; // 這樣可以讓TileAsset定義顏色;
    public Sprite customSprite; // TilePalette, TileMap用來顯示
    public bool isCollider = true; // 指定是否能通過
    public float rotateDegree = 0;
    public override void GetTileData(Vector3Int location, ITilemap tileMap, ref TileData tileData)
    {
        tileData.color = customColor;           // the main input 
        tileData.sprite = customSprite;         // mandatory to show something 
        tileData.colliderType = isCollider ? Tile.ColliderType.Grid : Tile.ColliderType.None;
        //transform >>>
        // Set the Matrix4x4 using Quaternion and SetTRS 
        Quaternion quaternion = Quaternion.Euler(0f, 0f, -rotateDegree);    // ken: rotate using Z-axis
                                                                            // degree > 0: clockwise 
        Matrix4x4 mat = Matrix4x4.identity;
        mat.SetTRS(Vector3.zero, quaternion, Vector3.one);
        tileData.transform = mat;// Define the new Matrix
        //transform <<<

        tileData.flags = TileFlags.LockAll;
    }

#if UNITY_EDITOR
    [MenuItem("Assets/Create/Custom Tile 2D")]
    public static void CreateTile()
    {
        string path = EditorUtility.SaveFilePanelInProject("Save CustomTile",
                              "CustomTile", "asset", "Save ColorTile", "Assets");
        if (path == "")
        {
            return;
        }

        AssetDatabase.CreateAsset(ScriptableObject.CreateInstance<CustomTile>(), path);
    }
#endif

}
