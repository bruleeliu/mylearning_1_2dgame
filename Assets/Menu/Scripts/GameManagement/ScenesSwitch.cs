﻿using System;
using System.Collections;
using System.Collections.Generic;
using Boo.Lang.Environments;
using UnityEngine;
using UnityEngine.Animations;
using UnityEngine.SceneManagement;

public class ScenesSwitch : MonoBehaviour
{
    public void Menu()
    {
        GameManagers.Instance.gameObject.GetComponent<MiscellaneousFunc>().RegisterSceneLoadMiscellaneousFunc();
        SceneManager.LoadScene("Menu"); //menu
    }
    public void QuitGame()
    {
        Application.Quit();
    }

    public void GoToNextScene(string nextScene, bool directlyWithoutFade)
    {
        if (directlyWithoutFade == true)
        {
            //RegisterScenesSwitchOnLoaded();
            SceneManager.LoadScene(nextScene);
        }
        else
        {
            GameObject myObject = GameObject.Find("FadeColor");
            if (myObject == null)
                return;

            if (GameManagers.Instance != null)
            {
                //Debug.Log("GoToNextScene do SceneTransitionAnimationEvent to " + nextScene);
                GameDataStruct.AllSubScenesList sceneEnum = GameManagers.Instance.CommunFunc.GetEnumBySubScenesName(nextScene);
                myObject.GetComponent<FadeControl>().SceneTransitionAnimationEvent(null, FadeControl.FadeMode.In, FadeControl.FadePlayMode.Play, sceneEnum);
            }
            else
                SceneManager.LoadScene(GameDataStruct.AllSubScenesList.Menu.ToString());
        }
    }

    //==========================================================================

    private void Awake()
    {
        RegisterScenesSwitchOnLoaded();
    }

    void Start()
    {
        //Debug.Log("sdfsdfs");
        //CheckNowScene();
    }
    //==========================================================================
    protected void RegisterScenesSwitchOnLoaded()
    {
        //Debug.Log("RegisterScenesSwitchOnLoaded");
        SceneManager.sceneLoaded += ScenesSwitchOnLoaded; //add register event on each scene load
    }

    void ScenesSwitchOnLoaded(UnityEngine.SceneManagement.Scene scene, LoadSceneMode mode)
    {
        //Debug.Log("SceneLoadScenesSwitch enter");

        //remove it for each screen load this when this object used DontDestroyOnLoad.
        //SceneManager.sceneLoaded -= ScenesSwitchOnLoaded; // Remove the delegate when no need.

        ///
        /// do things after scene loaded. (before start())
        /// 
        if (scene.name == GameDataStruct.AllSubScenesList.Menu.ToString())
            return;

        if (GameManagers.Instance == null)
        {
            SceneManager.LoadScene(GameDataStruct.AllSubScenesList.Menu.ToString());
            return;
        }

        if(GameManagers.Instance.GMState.GameStartInWhere == GameDataStruct.AllMainScenesList.SceneS2)
        {
            GameObject player = GameManagers.Instance.CommunFunc.FindDontDestroyObjectByName(GameDataStruct.AllCharactersList.S2Player.ToString());
            if (player == null)
            {
                Debug.Log("no player found in scene switch");
                int i = 0;
                foreach (GameDataStruct.DontDestroyList aa in GameManagers.Instance.DontDestroyArray)
                {
                    Debug.Log($"{i}: {aa.name}, object name={aa.instance.gameObject.name}");
                    i++;
                }
                SceneManager.LoadScene(GameDataStruct.AllSubScenesList.Menu.ToString());
                return;
            }

            GameDataStruct.CharactersData charactersData;

            GameDataStruct.AllSubScenesList sceneEnum = GameManagers.Instance.CommunFunc.GetEnumBySubScenesName(scene.name);
            switch (sceneEnum) // current scene
            {
                case GameDataStruct.AllSubScenesList.ScenesS2_Init:
                    /// init player data 
                    Player playerInfo = player.GetComponent<Player>();

                    playerInfo.Location = new Vector3((int)player.transform.position.x, (int)player.transform.position.y, player.transform.position.z);
                    playerInfo.FromSubScene = sceneEnum;
                    charactersData = new GameDataStruct.CharactersData(
                        playerInfo.Name, playerInfo.Hp, playerInfo.Mp, playerInfo.Others, playerInfo.Location,
                        playerInfo.MainScene, playerInfo.FromSubScene, playerInfo.FromPortal);
                    GameManagers.Instance.PlayerInfo.SetData(charactersData); // save to game manager

                    SceneManager.LoadScene(GameDataStruct.AllSubScenesList.ScenesS2_InMainTown.ToString());
                    break;
                case GameDataStruct.AllSubScenesList.ScenesS2_InMonsterRoom:
                case GameDataStruct.AllSubScenesList.ScenesS2_InMainTown:
                case GameDataStruct.AllSubScenesList.ScenesS2_InHouse:
                    charactersData = GameManagers.Instance.PlayerInfo.GetData(); // load from game manager
                    string fromPortalName = charactersData.FromPortal.ToString();
                    string fromSubSceneName = charactersData.FromSubScene.ToString();

                    if (scene.name == fromSubSceneName || (fromSubSceneName == GameDataStruct.AllSubScenesList.ScenesS2_Init.ToString()))
                        break;

                    switch (charactersData.FromPortal)
                    {
                        case GameDataStruct.AllPortalsList.HomePortal:
                        case GameDataStruct.AllPortalsList.End:
                        case GameDataStruct.AllPortalsList.Unknown:
                            //jump and do nothing
                            break;
                        default:
                            GameObject myPortal = GameObject.Find(fromPortalName);
                            Vector3 temp = charactersData.Location;
                            //Debug.Log("temp loc=" + temp);

                            temp.x = myPortal.transform.position.x;
                            temp.y = myPortal.transform.position.y;
                            //Debug.Log("modify temp loc=" + temp);

                            if (scene.name == GameDataStruct.AllSubScenesList.ScenesS2_InMainTown.ToString()) // current scene
                                temp.y -= 1f;
                            else if (scene.name == GameDataStruct.AllSubScenesList.ScenesS2_InHouse.ToString()) // current scene
                                temp.y += 1f;
                            else if (scene.name == GameDataStruct.AllSubScenesList.ScenesS2_InMonsterRoom.ToString()) // current scene
                            {
                                //temp.y += 1f;
                                temp.x = 0f;
                                temp.y = 0f;
                            }
                            player.transform.position = temp;
                            break;
                    }
                    break;
                default:
                    Debug.Log($"Now scene is {scene.name} and nothing to do");
                    break;
            }
        }
        
    }

    float CollisionDistanceBetweenTwoObjects(GameObject object1, GameObject object2)
    {
        float radius1 = object1.GetComponent<CircleCollider2D>().radius;
        float radius2 = object2.GetComponent<CircleCollider2D>().radius;

        return radius1 + radius2;
    }

    //==========================================================================


    //==========================================================================
}
