﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Loader : MonoBehaviour
{
    public bool EnableLoader = true;
    public GameObject GameManager;

    void Awake()
    {
        if (EnableLoader == false)
            return;
        if (GameManagers.Instance == null)
            Instantiate(GameManager);
    }
}
