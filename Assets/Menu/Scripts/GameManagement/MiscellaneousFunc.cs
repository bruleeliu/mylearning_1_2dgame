﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;

public class MiscellaneousFunc : MonoBehaviour
{
    private static GameObject mTipsObject;
    private const string TIP_NAME = "Tips";

    //===========================================================
    public void ButtonClickCheck(GameObject buttonObject)
    {
        string[] mButtonNameList = { "Game1Button", "Game2Button", "QuitButton", "QuestionMark", "TipsClose" };
        bool mObjectMatch = false;

        for(int i=0; i<mButtonNameList.Length; i++)
        {
            if (buttonObject.name == mButtonNameList[i])
            {
                //Debug.Log(buttonObject.name + "is matched");
                mObjectMatch = true;
                break;
            }
        }
        if (mObjectMatch == false) return;

        GameManagers.Instance.GMState.GameStartInWhere = GameDataStruct.AllMainScenesList.Menu;

        if (buttonObject.name == "Game2Button")
        {
            GameManagers.Instance.GMState.GameStartInWhere = GameDataStruct.AllMainScenesList.SceneS2;
            GameManagers.Instance.gameObject.GetComponent<ScenesSwitch>().GoToNextScene(GameDataStruct.AllSubScenesList.ScenesS2_Init.ToString(), true);
        }
        else if (buttonObject.name == "Game1Button")
        {
            GameManagers.Instance.GMState.GameStartInWhere = GameDataStruct.AllMainScenesList.SceneS1;
            GameManagers.Instance.gameObject.GetComponent<ScenesSwitch>().GoToNextScene(GameDataStruct.AllSubScenesList.ScenesS1.ToString(), true);
        }
        else if (buttonObject.name == "QuitButton")
            GameManagers.Instance.gameObject.GetComponent<ScenesSwitch>().QuitGame();
        else if (buttonObject.name == "TipsClose")
            TipsDisplayed();
        else if (buttonObject.name == "QuestionMark")
            TipsDisplayed();
            
    }
    public void RegisterSceneLoadMiscellaneousFunc()
    {
        SceneManager.sceneLoaded += SceneLoadMiscellaneousFunc;
    }
    //===========================================================
    private void Awake()
    {
        RegisterSceneLoadMiscellaneousFunc();
        InitTipsMenu();
    }
    //===========================================================

    void SceneLoadMiscellaneousFunc(UnityEngine.SceneManagement.Scene scene, LoadSceneMode mode)
    {
        // Remove it for each screen load this when this object used DontDestroyOnLoad.
        SceneManager.sceneLoaded -= SceneLoadMiscellaneousFunc; 

        // To Do...
        InitTipsMenu();
    }
    void InitTipsMenu()
    {
        if(mTipsObject == null)
            mTipsObject = GameObject.Find(TIP_NAME);
        //else
        //    Debug.Log("123123: "+mTipsObject.name);
        mTipsObject.SetActive(false);

        for (int i = 0; i < mTipsObject.transform.childCount; i++)
        {
            //Debug.Log("i="+i + "=> " + mTipsObject.transform.GetChild(i).name);
            if (mTipsObject.transform.GetChild(i).name == "ShowRange")
            {
                GameObject secondChild = mTipsObject.transform.GetChild(i).gameObject;
                for (int j = 0; j < secondChild.transform.childCount; j++)
                {
                    //Debug.Log("j=" + j + "=> " + secondchild.transform.GetChild(j).gameObject.name);
                    if (secondChild.transform.GetChild(j).gameObject.name == "ContentText")
                        secondChild.transform.GetChild(j).gameObject.GetComponent<Text>().text = Versions.TIP_MESSAGE;
                }
            }
        }
    }

    private void TipsDisplayed()
    {
        mTipsObject.SetActive(!mTipsObject.activeSelf);
    }
    //===========================================================
}
