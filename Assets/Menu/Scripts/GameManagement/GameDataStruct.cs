﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class GameDataStruct
{
    /// <summary>
    /// Struct definition
    /// </summary>
    #region enum definition
    public enum AllMainScenesList { Unknown=0, Menu, SceneS1, SceneS2, End};
    public enum AllSubScenesList { Unknown=0, Menu, ScenesS1, ScenesS2_InMainTown, ScenesS2_Init, ScenesS2_InHouse, ScenesS2_InMonsterRoom, End };
    public enum AllPortalsList { Unknown=0, HomePortal, ArmorPortal, ItemPortal, WeaponPortal, MonsterRoomPortal, End };
    public enum AllCharactersList { Unknown=0, S2Player, End };
    public enum AllGameUIList { Unknown=0, PlayerUI, End};
    #endregion
    
    #region public struct definition
    public struct ManagerState
    {
        //public bool DoingSetup;
        public AllMainScenesList GameStartInWhere;

        public ManagerState(/*bool doingSetup,*/ AllMainScenesList startWhere)
        {
            //DoingSetup = doingSetup;
            GameStartInWhere = startWhere;
        }
    }
    public struct CharactersData
    {
        [Serializable]public struct Attributes
        {
            public float Atk;
            //........
        }

        private string mName;
        private Vector2 mHp; //(current, max)
        private Vector2 mMp; //(current, max)
        private Attributes mOthers;
        private Vector3 mLocation;
        private AllMainScenesList mMainScene;
        private AllSubScenesList mFromSubScene;
        private AllPortalsList mFromPortal;
        public CharactersData(string name, Vector2 hp, Vector2 mp, Attributes others, Vector3 location, AllMainScenesList mainScene, AllSubScenesList subScene, AllPortalsList portal)
        {
            mName = name;
            mHp = hp;
            if (mHp.x < 0f) mHp.x = 0f;
            if (mHp.y < 0f) mHp.y = 0f;
            mMp = mp;
            if (mMp.x < 0f) mMp.x = 0f;
            if (mMp.y < 0f) mMp.y = 0f;
            mOthers = others;
            mLocation = location;
            mMainScene = mainScene;
            mFromSubScene = subScene;
            mFromPortal = portal;
        }
        public string Name { get => mName; }
        public Vector2 Hp { get => mHp; }
        public Vector2 Mp { get => mMp; }
        public Attributes Others { get => mOthers; }
        public Vector3 Location { get => mLocation; }
        public AllMainScenesList MainScene { get => mMainScene; }
        public AllSubScenesList FromSubScene { get => mFromSubScene; }
        public AllPortalsList FromPortal { get => mFromPortal; }
        public override string ToString() => $"name={Name}, hp={Hp}, mp={Mp}, others={Others}, location={Location}, main scene={MainScene}, from scene={FromSubScene}, from portal={FromPortal}";

        public CharactersData GetData()
        {
            //if (mHp.x < 0f) mHp.x = 0f;
            //if (mHp.y < 0f) mHp.y = 0f;
            //if (mMp.x < 0f) mMp.x = 0f;
            //if (mMp.y < 0f) mMp.y = 0f; 
            CharactersData charactersData = new CharactersData(mName, mHp, mMp, mOthers, mLocation, mMainScene, mFromSubScene, mFromPortal);
            return charactersData;
        }

        public void SetData(CharactersData data)
        {
            mName = data.Name;
            mHp = data.Hp;
            if (mHp.x < 0f) mHp.x = 0f;
            if (mHp.y < 0f) mHp.y = 0f;
            mMp = data.Mp;
            if (mMp.x < 0f) mMp.x = 0f;
            if (mMp.y < 0f) mMp.y = 0f;
            mOthers = data.Others;
            mLocation = data.Location;
            mMainScene = data.MainScene;
            mFromSubScene = data.FromSubScene;
            mFromPortal = data.FromPortal;
        }

    }
    public struct RogueLikeMap
    {
        public struct MapSize
        {
            public int Columns;
            public int Rows;

            public MapSize(int col, int row)
            {
                Columns = col;
                Rows = row;
            }
        }

        private int mRoomLevel;
        private int mEnemiesCount;
        private int mItemsCount;
        private int mObstaclesCount;
        private MapSize mRoomSize;

        private BoardManager mBoardScript;

        public int RoomLevel { get => mRoomLevel; set => mRoomLevel = value; }
        public int EnemiesCount { get => mEnemiesCount; }
        public int ItemsCount { get => mItemsCount; }
        public int ObstaclesCount { get => mObstaclesCount; }
        public MapSize RoomSize { get => mRoomSize; }

        //public RogueLikeMap(MapSize size, int roomLevel, int enemyCount, int itemCount, int obstacleCount)
        //{
        //    mRoomSize = size;
        //    mRoomLevel = roomLevel;
        //    mEnemiesCount = enemyCount;
        //    mItemsCount = itemCount;
        //    mObstaclesCount = obstacleCount;
        //}
        public void InitMap(BoardManager boardManager)
        {
            mBoardScript = boardManager;
            mRoomSize = new MapSize(8, 8);
            mRoomLevel = 1;
            mEnemiesCount = 0;
            mItemsCount = 0;
            mObstaclesCount = 0;
        }
        public void StartMap(int level)
        {
            if(mBoardScript == null)
            {
                Debug.Log("Map not be initialize first");
                return;
            }
            mBoardScript.SetupScene(level);
        }
        public void SetMapParameters(MapSize size, int roomLevel, int enemyCount, int itemCount, int obstacleCount)
        {
            mRoomSize = size;
            mRoomLevel = roomLevel;
            mEnemiesCount = enemyCount;
            mItemsCount = itemCount;
            mObstaclesCount = obstacleCount;
        }

        public override string ToString() => $"(Room LV.={RoomLevel}, Enemy={EnemiesCount}, Item={ItemsCount}, Obstacle={ObstaclesCount}, Room Size)=({RoomSize.Columns},{RoomSize.Rows})";
    }
    public struct MapInformation
    {
        public RogueLikeMap Map1;
    }
    public struct DontDestroyList
    {
        public string name;
        public GameObject instance;
        public DontDestroyList(string objectName, GameObject objectInstance)
        {
            name = objectName;
            instance = objectInstance;
        }
    }
    public struct EnemyInformation
    {
        public List<Enemy> EnemiesList;
        public bool PlayersTurn;
    }
    #endregion
    //============================================================================================
    /// <summary>
    /// Global parameter declaration
    /// </summary>
    //public CharactersData PlayerData;
    //============================================================================================

    /// <summary>
    /// Supported method declaration
    /// </summary>

    public GameDataStruct()
    {
        //PlayerData = new CharactersData();
    }

    //public int GetIndexByScenesName(string sceneName)
    //{
    //    int resultIndex = (int)AllScenesList.End;

    //    foreach (AllScenesList val in Enum.GetValues(typeof(AllScenesList)))
    //    {
    //        //print("val value is " + (int)val);
    //        //print("val string is " + Enum.GetName(typeof(AllPortalGateList), val).ToString());
    //        if (sceneName == val.ToString())
    //        {
    //            resultIndex = (int)val;
    //            break;
    //        }
    //    }
    //    return resultIndex;
    //}

    public AllMainScenesList GetEnumByMainScenesName(string sceneName)
    {
        AllMainScenesList sceneEnumName = AllMainScenesList.Unknown;

        foreach (AllMainScenesList val in Enum.GetValues(typeof(AllMainScenesList)))
        {
            //print("val value is " + (int)val);
            //print("val string is " + Enum.GetName(typeof(AllPortalGateList), val).ToString());
            if (sceneName == val.ToString())
            {
                sceneEnumName = val;
                break;
            }
        }

        return sceneEnumName;
    }

    public AllSubScenesList GetEnumBySubScenesName(string sceneName)
    {
        AllSubScenesList sceneEnumName = AllSubScenesList.Unknown;

        foreach (AllSubScenesList val in Enum.GetValues(typeof(AllSubScenesList)))
        {
            //print("val value is " + (int)val);
            //print("val string is " + Enum.GetName(typeof(AllPortalGateList), val).ToString());
            if (sceneName == val.ToString())
            {
                sceneEnumName = val;
                break;
            }
        }

        return sceneEnumName;
    }

    //public int GetIndexByPortalName(string portalName)
    //{
    //    int resultIndex = (int)AllPortalsList.End;

    //    foreach (AllPortalsList val in Enum.GetValues(typeof(AllPortalsList)))
    //    {
    //        //print("val value is " + (int)val);
    //        //print("val string is " + Enum.GetName(typeof(AllPortalGateList), val).ToString());
    //        if (portalName == val.ToString())
    //        {
    //            resultIndex = (int)val;
    //            break;
    //        }
    //    }
    //    return resultIndex;
    //}
    
    public AllPortalsList GetEnumByPortalName(string portalName)
    {
        AllPortalsList result = AllPortalsList.Unknown;

        foreach (AllPortalsList val in Enum.GetValues(typeof(AllPortalsList)))
        {
            //print("val value is " + (int)val);
            //print("val string is " + Enum.GetName(typeof(AllPortalGateList), val).ToString());
            if (portalName == val.ToString())
            {
                result = val;
                break;
            }
        }
        return result;
    }

    public Vector2 CheckHpMp(Vector2 hpMp)
    {
        if (hpMp.x < 0f) hpMp.x = 0f;
        if (hpMp.y < 0f) hpMp.y = 0f;
        //if (hpMp.y < hpMp.x) hpMp.y = hpMp.x;
        return hpMp;
    }

    public GameObject FindDontDestroyObjectByName(string inputName)
    {
        if (GameManagers.Instance == null)
            return null;
        if (GameManagers.Instance.DontDestroyArray == null || GameManagers.Instance.DontDestroyArray.Count <= 0)
        {
            Debug.Log("No item can be found");
            return null;
        }
        foreach (DontDestroyList list in GameManagers.Instance.DontDestroyArray)
            if (inputName == list.name)
                return list.instance;
        return null;
    }

    public bool AddDontDestroyObjectToList(DontDestroyList addObject)
    {
        if (GameManagers.Instance == null)
            return false;
        GameManagers.Instance.DontDestroyArray.Add(addObject);
        return true;
    }

    public bool RemoveDontDestroyObjectByName(string inputName)
    {
        if (GameManagers.Instance == null)
            return false;
        if (GameManagers.Instance.DontDestroyArray == null || GameManagers.Instance.DontDestroyArray.Count <= 0)
        {
            Debug.Log("no item can be removed");
            return false;
        }
        int i = 0;
        foreach(DontDestroyList list in GameManagers.Instance.DontDestroyArray)
        {
            if(list.name == inputName)
            {
                UnityEngine.Object.Destroy(list.instance);
                GameManagers.Instance.DontDestroyArray.RemoveAt(i);
                return true;
            }
            i++;
        }
        return false;
    }

    public Vector3 BoundaryCheck2D(Vector3 current, Vector3 minBoundary, Vector3 maxBoundary)
    {
        Vector3 result = current;

        result.x = Mathf.Clamp(current.x, minBoundary.x, maxBoundary.x);
        result.y = Mathf.Clamp(current.y, minBoundary.y, maxBoundary.y);
        return result;
    }
}