﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Versions : MonoBehaviour
{
    public const string GAME_VERSION = "v0.03 beta";
    public const string TIP_MESSAGE = "預定完成項目:"
    + "\n" + "1. 在Menu增加提示清單 (完成)"
    + "\n" + "2. Sample2 UI自適應 (完成)"
    + "\n" + "3. 調整Sample2 camera邊界 (完成)"
    + "\n" + "4. 整調變數命名 (完成)"
    + "\n" + "5. 整調Module使用方式 (完成)"
    + "\n" + "6. 加入角色name,hp,mp UI (完成)"
    + "\n" + "7. 修改fade模組的用法 (完成)"
    + "\n" + "8. 更新fade模組 (完成)"
    + "\n" + "9. Player UI模組轉prefabs (完成)"
    + "\n" + "10. 更新GameManager模組 (完成)"
    + "\n" + "11. 更新S2Player模組 (完成)"
    + "\n" + "12. 更新GameManager模組#2 (完成)"
    + "\n" + "13. 更新ScenesSwitch和Fade模組#2 (完成)"
    + "\n" + "14. GameManager增加miscellaneous功能 (完成)"
    + "\n" + "15. 更新Player相關模組 (完成)"
    + "\n" + "16. 調整碰撞方式 (完成)"
    + "\n" + "17. 加入怪物房 (完成)"
    + "\n" + "18. Camera小調整 (完成)"
    + "\n" + "19. 更新Game Manager, Player, UI模組 (完成)"
    + "\n" + "20. 更新怪物房機制 (完成)"
    + "\n" + "21. 更新Game Manager, Player, UI模組#2 (完成)"
    + "\n" + "22. 怪物房死亡處理 (未完成)"
    + "\n" + "23. 加入sample1 game(簡易版) (完成)"
    + "";

    // Start is called before the first frame update
    void Start()
    {
        GameObject.Find("GameVersion").GetComponent<Text>().text = GAME_VERSION;
    }

    
}
