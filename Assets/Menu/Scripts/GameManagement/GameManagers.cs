﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManagers : MonoBehaviour
{
    #region public variables
    public static GameManagers Instance = null;
    public GameDataStruct.ManagerState GMState = new GameDataStruct.ManagerState(GameDataStruct.AllMainScenesList.Menu);

    public GameDataStruct CommunFunc = new GameDataStruct();
    public GameDataStruct.MapInformation MapInfo = new GameDataStruct.MapInformation();
    public GameDataStruct.CharactersData PlayerInfo = new GameDataStruct.CharactersData();
    public List<GameDataStruct.DontDestroyList> DontDestroyArray = new List<GameDataStruct.DontDestroyList>();
    public GameDataStruct.EnemyInformation EnemyInfo = new GameDataStruct.EnemyInformation();
    #endregion


    void Awake()
    {
        //Debug.Log("Game Manager Init");
        if(Instance != null)
            GameManagers.Instance.DontDestroyArray.Clear();
        DontDestroyGameObject();
        InitGame();
        RegisterGameManagersOnLoaded();
    }
    private void Update()
    {

    }
    //=============================================================
    protected void RegisterGameManagersOnLoaded()
    {
        //Debug.Log("RegisterGameManagersOnLoaded");
        SceneManager.sceneLoaded += GameManagersOnLoaded; //add register event on each scene load
    }
    //=============================================================

    void GameManagersOnLoaded(UnityEngine.SceneManagement.Scene scene, LoadSceneMode mode)
    {
        //Debug.Log("GameManagersOnLoaded enter");
        //remove it for each screen load this when this object used DontDestroyOnLoad.
        //SceneManager.sceneLoaded -= PlayerOnLoaded; 

        if (GMState.GameStartInWhere == GameDataStruct.AllMainScenesList.SceneS2)
        {
            if (scene.name == GameDataStruct.AllSubScenesList.ScenesS2_Init.ToString())
            {
                // Init game objects for scene sample 2

                // Try to get player
                GameObject player = GameObject.Find(GameDataStruct.AllCharactersList.S2Player.ToString());
                if (player != null)
                {
                    DontDestroyOnLoad(player);
                    GameDataStruct.DontDestroyList temp = new GameDataStruct.DontDestroyList(GameDataStruct.AllCharactersList.S2Player.ToString(), player);
                    GameManagers.Instance.CommunFunc.AddDontDestroyObjectToList(temp);
                }

                // Try to get player UI
                GameObject playerUI = GameObject.Find(GameDataStruct.AllGameUIList.PlayerUI.ToString());
                if (playerUI != null)
                {
                    DontDestroyOnLoad(playerUI);
                    GameDataStruct.DontDestroyList temp = new GameDataStruct.DontDestroyList(GameDataStruct.AllGameUIList.PlayerUI.ToString(), playerUI);
                    GameManagers.Instance.CommunFunc.AddDontDestroyObjectToList(temp);
                }
            }

            if (scene.name == GameDataStruct.AllSubScenesList.ScenesS2_InMonsterRoom.ToString())
            {
                // Init monster room
                //MapInfo.Map1.InitMap(GetComponent<BoardManager>());
                MapInfo.Map1.StartMap(MapInfo.Map1.RoomLevel);
                Debug.Log("Map Info ->" + MapInfo.Map1.ToString());
            }
        }
    }

    void DontDestroyGameObject()
    {
        if (Instance == null)
        {
            //Debug.Log("Instance=null, this=" + this);
            Instance = this;
        }
        else if (Instance != this)
        {
            //Debug.Log("Instance=" + Instance + " this=" + this);
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }

    private void InitGame()
    {
        MapInfo.Map1.InitMap(GetComponent<BoardManager>());
        //MapInfo.Map1.RoomLevel = 2;
        //Debug.Log(MapInfo.Map1.ToString());
        EnemyInfo.EnemiesList = new List<Enemy>();
        EnemyInfo.EnemiesList.Clear();
        EnemyInfo.PlayersTurn = true;
        
    }
    //=============================================================
   
    //=============================================================
}
