﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
//using UnityEditor.Experimental.TerrainAPI;
#if false//UNITY_EDITOR
using UnityEditor;
#endif


public class FadeControl : MonoBehaviour
{
    public enum FadeMode { Unknown, In, Out };
    public enum FadePlayMode { Unknown, Play, Stop, None };


    private GameObject mFadeObject = null;
    private string mFadeObjectName = "FadeColor";
    private static GameDataStruct.AllSubScenesList mGoToNextScene = GameDataStruct.AllSubScenesList.Unknown;

    [SerializeField] public bool IsEnable = true;
    [SerializeField] public bool PlayDefaultFadeIn = true;
    [SerializeField] public bool DefaultBlackTop = true;
    //===========================================================

    private void Awake()
    {
        IsGetDefaultFadeGameObject();
        FadeObjectInit();
        //DefaultFadeInPlayInDefault(FadeState.In, PlayDefaultFadeIn);
    }

    //===========================================================
    /// <summary>
    /// Public functions
    /// </summary>

    public void AnimePlayEndEvent()
    {
        if (mGoToNextScene != GameDataStruct.AllSubScenesList.Unknown)
        {
            SceneManager.LoadScene(mGoToNextScene.ToString());
        }    
    }

    public void SceneTransitionAnimationEvent(string fadeAnime, FadeMode fadeMode, FadePlayMode fadePlayMode, GameDataStruct.AllSubScenesList nextScene)
    {
        string currentScene = SceneManager.GetActiveScene().name;
        //Debug.Log("SceneTransitionAnimationEvent: (" + currentScene + " -> " + nextScene.ToString() + ")\n" + fadeAnime + "\n" + fadeMode + "\n" + fadePlayMode);

        if (IsGetDefaultFadeGameObject() == false)
            return;

        //play fade anime
        Animation anima = mFadeObject.GetComponent<Animation>();
        if (fadeMode == FadeMode.In)
            fadeAnime = "FadeIn";
        else if (fadeMode == FadeMode.Out)
            fadeAnime = "FadeOut";
        else
        {
            if (IsAnimationElement(anima, fadeAnime) == false)
                return;
        }
        if (fadePlayMode == FadePlayMode.Play)
        {
            PlayModeSet(FadePlayMode.Play);
            anima.Play(fadeAnime);
        }
        else if(fadePlayMode == FadePlayMode.Stop)
        {
            PlayModeSet(FadePlayMode.Stop);
            anima.Stop(fadeAnime);
        }

        mGoToNextScene = GameDataStruct.AllSubScenesList.Unknown;
        //transfer to next scene
        if (currentScene != nextScene.ToString())
        {
            //Debug.Log("FadeControl goes to next scene (" + nextScene.ToString() + ")");
            mGoToNextScene = nextScene;
            //GameManagers.Instance.gameObject.GetComponent<ScenesSwitch>().RegisterTriggerSceneLoad();
            //SceneManager.LoadScene(nextScene.ToString());
            GameManagers.Instance.gameObject.GetComponent<ScenesSwitch>().GoToNextScene(nextScene.ToString(), true);
        }
    }

    public void FadInPlayMode(FadePlayMode playMode)
    {
        PlayModeSet(playMode);
    }

    public void EventJumpToNextScene(string sceneName)
    {
        SceneManager.LoadScene(sceneName);
    }
    //===========================================================

    bool IsGetDefaultFadeGameObject()
    {
        if (mFadeObjectName != this.name) //set fade name as game object name who use it;
            mFadeObjectName = this.name;
        if (mFadeObject == null)
            mFadeObject = GameObject.Find(mFadeObjectName) as GameObject; //get fade object
        if (mFadeObject == null)
            return false;
        else
            return true;
    }

    void FadeObjectInit()
    {
        if (IsEnable == false)
            return;
        
        if (IsGetDefaultFadeGameObject() == false)
            return;

        Animation anima = mFadeObject.GetComponent<Animation>();
        anima.playAutomatically = false;
        //ElementIndex = Mathf.Clamp(ElementIndex, 0, anima.GetClipCount()); // clamp animation element index
        //string elementName = ClipIndexToName(anima, ElementIndex);

        //Debug.Log(anima.clip); //play for playAutomatically = true
        if (DefaultBlackTop == true)
            PlayModeSet(FadePlayMode.Play);
        else
            PlayModeSet(FadePlayMode.Stop);

        if (PlayDefaultFadeIn == true)
        {
            string currentScene = SceneManager.GetActiveScene().name;

            if (GameManagers.Instance == null)
            {
                GameDataStruct tempGameData = new GameDataStruct();
                GameDataStruct.AllSubScenesList sceneEnum = tempGameData.GetEnumBySubScenesName(currentScene);
                SceneTransitionAnimationEvent(null, FadeMode.In, FadePlayMode.Play, sceneEnum);
            }
            else
            {
                GameDataStruct.AllSubScenesList sceneEnum = GameManagers.Instance.CommunFunc.GetEnumBySubScenesName(currentScene);
                SceneTransitionAnimationEvent(null, FadeMode.In, FadePlayMode.Play, sceneEnum);
            }
        }
    }

    void PlayModeSet(FadePlayMode playMode)
    {
        if (IsGetDefaultFadeGameObject() == false)
            return;
        if (mFadeObject.transform.parent == null)
            return;
        
        //Debug.Log(myObject.transform.parent.gameObject.name);

        if (playMode == FadePlayMode.Stop)
        {
            mFadeObject.transform.parent.gameObject.GetComponent<Canvas>().sortingOrder = -100;
            mFadeObject.transform.SetAsFirstSibling();
            mFadeObject.GetComponent<CanvasGroup>().alpha = 0;
            mFadeObject.transform.parent.gameObject.GetComponent<Canvas>().sortingLayerName = "Default";
        }
        else if (playMode == FadePlayMode.Play)
        {
            mFadeObject.transform.parent.gameObject.GetComponent<Canvas>().sortingOrder = 100;
            mFadeObject.transform.SetAsLastSibling();
            mFadeObject.GetComponent<CanvasGroup>().alpha = 1;
            mFadeObject.transform.parent.gameObject.GetComponent<Canvas>().sortingLayerName = "UI";
        }
        
    }

    bool IsAnimationElement(Animation anima, string name)
    {
        foreach(AnimationState animationState in anima)
        {
            if (name == animationState.clip.name)
                return true;
        }
        return false;
    }

    string ClipIndexToName(Animation anima, int index)
    {
        AnimationClip clip = GetClipByIndex(anima, index);
        if (clip == null)
            return null;
        return clip.name;
    }

    AnimationClip GetClipByIndex(Animation anima, int index)
    {
        int i = 0;
        foreach (AnimationState animationState in anima)
        {
            if (i == index)
                return animationState.clip;
            i++;
        }
        return null;
    }

    void TestFunction()
    {
        //Debug.Log("TestFunction enter");
        //if (mFadeObject == null)
        //    mFadeObject = GameObject.Find(mFadeObjectName) as GameObject; //get fade object
        //if (IsEnable == true)
        //{
        //    if( ElementIndex == -7)
        //    {
        //        mFadeObject.transform.parent.gameObject.GetComponent<Canvas>().sortingOrder = -100;
        //        mFadeObject.transform.SetAsFirstSibling();
        //        mFadeObject.GetComponent<CanvasGroup>().alpha = 0;
        //    }
        //    else if( ElementIndex == -71)
        //    {
        //        mFadeObject.transform.parent.gameObject.GetComponent<Canvas>().sortingOrder = 100;
        //        mFadeObject.transform.SetAsLastSibling();
        //        mFadeObject.GetComponent<CanvasGroup>().alpha = 1;
        //    }
        //}
        //Debug.Log("TestFunction exit");
    }
}


#if false//UNITY_EDITOR
[CustomEditor(typeof(FadeControl))]
public class FadeControlEditor : Editor
{
    private FadeControl fade { get { return (target as FadeControl); } }

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        EditorGUILayout.LabelField("===========");

        EditorGUI.BeginChangeCheck();
        int count = EditorGUILayout.DelayedIntField("Number of Elements", fade.FadeElements != null ? fade.FadeElements.Length : 0);
        if (count < 0)
            count = 0;

        if(fade.FadeElements == null || fade.FadeElements.Length != count)
        {
            Array.Resize<FadeControl.FadeParameter>(ref fade.FadeElements, count);
        }
        if (count == 0)
            return;

        //EditorGUILayout

        //EditorGUILayout.LabelField("Place element shown based on the order of animation.");
        EditorGUILayout.Space();

        for(int i=0; i<count; i++)
        {
            EditorGUILayout.LabelField("Element " + i.ToString());
            fade.FadeElements[i].Name = EditorGUILayout.TextField("    Name", fade.FadeElements[i].Name);
            fade.FadeElements[i].IndexInAnimations = EditorGUILayout.IntField("    Index of Animations", fade.FadeElements[i].IndexInAnimations);
            fade.FadeElements[i].PlayInDefault = EditorGUILayout.Toggle("    Play Automatically", fade.FadeElements[i].PlayInDefault);
            fade.FadeElements[i].Direction = (FadeControl.FadeState)EditorGUILayout.EnumPopup("    Direction", fade.FadeElements[i].Direction);
            fade.FadeElements[i].Mode = (FadeControl.PlayMode)EditorGUILayout.EnumPopup("    Play Mode", fade.FadeElements[i].Mode);
        }

        if (EditorGUI.EndChangeCheck())
            EditorUtility.SetDirty(fade);
    }
}
#endif
